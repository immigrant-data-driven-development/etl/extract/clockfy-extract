from kafka import KafkaProducer
from decouple import config
import json
producer = KafkaProducer(bootstrap_servers=config('SERVERS'),value_serializer=lambda v: json.dumps(v).encode('utf-8'))
data = {
        "secret": config('SECRET'),        
        }
producer.send(config('TOPIC'), data)
producer.flush()
print ("send - {} {}".format(config('SERVERS'),config('TOPIC')))
print (data)

