from decouple import config
from kafka import KafkaConsumer, KafkaProducer
import concurrent.futures
from clockify import factories as factory
from pprint import pprint
import json

consumer = KafkaConsumer(
    config('TOPIC'),
    group_id=config('GROUP_ID'),
    bootstrap_servers=config('SERVERS'),
    auto_offset_reset='earliest',  
    enable_auto_commit=True,  
    value_deserializer=lambda x: json.loads(x.decode('utf-8'))
    )

def preprocess_data(data):
    if isinstance(data, dict):
        return {str(k): preprocess_data(v) for k, v in data.items()}
    elif isinstance(data, list):
        return [preprocess_data(item) for item in data]
    elif isinstance(data, (str, int, float, bool)) or data is None:
        return data
    else:
        return str(data)

def publish (**kwargs):
    pprint ("topic {}".format(kwargs["topic"]))
    pprint ("-"*50)

    data = preprocess_data(kwargs["data"])

    producer = KafkaProducer(bootstrap_servers=config('SERVERS'),value_serializer=lambda v: json.dumps(v).encode('utf-8'))
    producer.send(kwargs["topic"], data)
    producer.flush()

try:
    for message in consumer:
        
        mensagem = message.value
        
        api_key = mensagem["secret"]

        workspace_services = factory.WorkspaceFactory(api_key=api_key)
        #project_services = factory.ProjectFactory(api_key=api_key)
        #tasks_services = factory.TaskFactory(api_key=api_key)
        timeentry_services = factory.TimeEntryFactory(api_key=api_key)
        #user_services = factory.UserFactory(api_key=api_key)

        workspaces = workspace_services.get_all_workspaces()

        for workspace in workspaces:
            pprint (workspace['name'])
            time_entries = timeentry_services.get_by_workspace_function(workspace,function=publish, topic="application.{}.{}".format("clockify","timeentries"), extra_data = None, last_day=True)

        """extract = {
            "boards"                : factory.BoardFactory(user=username,apikey=apikey,server=server)
        }

        with concurrent.futures.ProcessPoolExecutor(max_workers=100) as executor:
            list(executor.map(extract_function, projects,  [extract]*len(projects)))"""


except KeyboardInterrupt:
    print ("Interrupção do teclado detectada. Encerrando o consumidor")
finally:
    consumer.close()